\documentclass{beamer}
\include{preamble}

%Information to be included in the title page:
\title{Introduction to Neural Methods in NLP}
\subtitle{Recent advances in Deep Learning}
\author{Andrés Lou}
\date{2021}

\logo{
	\scalebox{.2}{\input{images/umaneo_logo.pdf_tex}}
	}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame}

\section{Introduction: The problem of modelling language}

\begin{frame}
	\frametitle{Introduction}
	\begin{itemize}
		\item<1-> Language is a natural phenomenon that is, to the best of our knowledge, unique to humans.
		\item<2-> We are very, \textit{very} good at language.
		\item<3-> So good, in fact, that it can be hard to remember how complex language actually is.
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Introduction}
	\begin{itemize}
		\item <1-> If you were trying to have a computer \textbf{understand language} you must remember that all a computer can do is \textbf{tons of computations, very quickly}.
		\item <3-> So if you want it to understand language, you can't just speak or write to it. You need a suitable language representation.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Introduction}
	\begin{block}{Definition}
	A \textbf{language representation} is a mathematical object that approximates and condenses the richness of natural language so that a computer may process it.
	\end{block}
\end{frame}


\begin{frame}
	\frametitle{Language features}
	\begin{example}
		\textit{«Deux hommes entre tous les hommes ont le droit de répondre maintenant. Le capitaine Nemo et moi.»}
	\end{example}
		\only<2>{Couple of grammar features:}
		\uncover<3->{Couple of \sout{grammar} linguistic features:}
		\begin{itemize}
			\item <4-> Lexical: “deux”, “hommes”, “entre”, etc
			\item <5-> Morpho-syntactic: Gender, number, inflexion, etc
			\item <6-> Semantic: “tous les hommes”? Does that include women?
			\item <7-> Pragmatic: \textit{Les deux hommes ont le droit \textbf{maintenant}}, which suggests they could not answer before.
			\item <8-> Discourse: “Les deux hommes” = “Le capitaine Nemo et moi”
		\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Language features}
	\begin{example}
		\textit{«Deux hommes entre tous les hommes ont le droit de répondre maintenant. Le capitaine Nemo et moi.»}
	\end{example}
	This is just for the \textit{written} form of the example!
	\begin{itemize}
		\item <2-> Intonation
		\item <3-> Prosody
		\item <4-> Dialect
		\item <5-> Language competence
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Language features}
	\begin{block}{Bottom line}
		Language is complex, and finding a suitable representation is no small feat.
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{MOBILIS IN MOBILI}
	\centering
	\includegraphics[width=.45\textwidth]{jules_verne}
\end{frame}


\section{Modern Neural Methods}


\begin{frame}
	\frametitle{Downstream tasks}
	Neural methods in NLP are mostly concerned with using feature-rich language representations to perform a variety of \textbf{downstream tasks}, including, but not limited to:
	\begin{itemize}
		\item<2-> \textbf<2>{Text Classification}\only<2>{: Given a sentence, a phrase, or a document, classify it using one or more categories.}
		\item<3-> \textbf<3>{Machine Translation}\only<3>{: Given a text in one language, translate it into another language.}
		\item<4-> \textbf<4>{Question Answering}\only<4>{: Given a question and an accompanying text that contains the answer, extract the answer.}
		\item<5-> \textbf<5>{Next-sentence prediction}\only<5>{: Given a sentence, predict the corresponding following sentence in terms of logic, discourse, syntax, etc.}
	\end{itemize}
\end{frame}

\def\title_word2vec{Word vectors: One-hot vs Dense}


\begin{frame}
	\frametitle{\title_word2vec}
	\begin{itemize}
		\item <1-> The easiest way to encode a sentence is to build a vocabulary and represent each word in the sentence as an index.
		\item <2-> This is called the \textbf{one-hot vector representation}
	\end{itemize}

	\uncover<3->{
		\begin{example}
			« Le chat est sur le tapis. » \only<5>{$\rightarrow [2, 0, 1, 3, 2, 4]$} \\
			\uncover<4->{$V = \{ \text{chat}, \text{est}, \text{le}, \text{sur}, \text{tapis} \}$}
		\end{example}
	}
\end{frame}


\begin{frame}
	\frametitle{\title_word2vec}
	\begin{itemize}
		\item <1-> One-hot vectors are not very informative, because they do not reflect anything other than the word itself.
		\item <2-> They say nothing regarding syntax, semantics, etc
	\end{itemize}

	
\end{frame}


\begin{frame}
	\frametitle{\title_word2vec}
	Mikolov et al (2014) introduced \textbf{Word2Vec}, a neural model capable of representing words not as indices, but instead as \textbf{dense vectors}

	\begin{example}
		« Le chat est sur le tapis. » \\
		\vspace{5pt} \pause
		$\text{vector}(\text{"Le"}) = [0.2589, 0.1757, 0.6877, 0.0817, \hdots]$	\\ \pause
		$\text{vector}(\text{"chat"}) = [0.1257, 0.1337, 0.7373, 0.8356, \hdots  ]$	\\ \pause
		$\text{vector}(\text{"est"}) = [0.6768, 0.5134, 0.3220, 0.0469, \hdots  ]$	\\ \pause
		$\text{vector}(\text{"sur"}) = [0.9259, 0.7515, 0.4842, 0.0701, \hdots  ]$	\\ \pause
		$\text{vector}(\text{"tapis"}) = [0.9820, 0.4070, 0.0156, 0.9608, \hdots  ]$	\\
	\end{example}
\end{frame}


\begin{frame}
	\frametitle{\title_word2vec}
	Why is this a big deal?

	\uncover<2->{Because now we can perform arithmetic with these vectors and represent \textbf{meaningful relations} between words}

	\[
		\uncover<3->{\text{vector}(\text{“king”}) - \text{vector}(\text{“man”}) + \text{vector}(\text{“woman”})} \uncover<4->{= \text{vector}(\text{“queen”})}
	\]

	\[
		\uncover<5->{\text{vector}(\text{“doctor”}) - \text{vector}(\text{“man”}) + \text{vector}(\text{“woman”})} \uncover<6->{= \text{vector}(\text{“nurse”}) \text{\includegraphics[width=10pt]{facepalm}}}
	\]
	% \uncover<6->{\includegraphics[scale=.1]{facepalm}}
\end{frame}


\begin{frame}
	\frametitle{\title_word2vec}
	\begin{block}{Beyond word-level embeddings}
	The idea word2vec can be expanded to produce not only word embeddings, but also \textbf{sentence embeddings} and even \textbf{document embeddings}.
	\end{block}
\end{frame}


\def\title_encoder_decoder{Encoder-Decoder models}


\begin{frame}
	\frametitle{\title_encoder_decoder}
	\begin{itemize}
		\item<1->One of the mayor hurdles of handling text is that word sequences as not fixed to any length.
		\item<2->A \textbf{sequence-to-sequence} model, also known as an \textbf{encoder-decoder} is a special neural architecture that transforms a sequence of any length into another sequence of any length.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\title_encoder_decoder}
	\begin{figure}
		\centering
		\scalebox{.7}{
			\input{images/encoder_decoder}
		}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\title_encoder_decoder}
	\begin{itemize}
		\item If we use a single \textbf{context vector} to encode the entire sequence, it acts as an information bottleneck.
		\item <2-> Instead we could tell the decoder which part of the input sequence to focus on when producing the next output step.
		\item <3->We can tell it what part of the input sequence to pay \textbf{attention} to.
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{\title_encoder_decoder}
	\begin{figure}
		\scalebox{.7}{
			\input{images/attn1}
		}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{\textit{The road goes ever on and on}}
	\centering
	\includegraphics[width=.45\textwidth]{jrr_tolkien}
\end{frame}


\def\title_transformer{Transformers: Vectors in disguise}
\section{Transformers and the state of the art}


\begin{frame}
	\frametitle{Transformers: vEctOrS iN dIsGuIsE}
	\only<2>{
		\centering
		\includegraphics[scale=.4]{optimus_prime}
	}
	\only<3>{}
	\begin{itemize}
		\item <4-> The \textbf{Transformer} is another kind of Encoder-Decoder model.
		\item <5-> It moves away from the sequential nature of recurrent networks, while keeping and reimplementing the concept of Attention.
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{\title_transformer}
	\centering
	\only<1>{\includegraphics[scale=.4]{optimus_prime}}
	\only<2>{
		\scalebox{.55}{
			\input{images/transformer}
		}
	}
\end{frame}


\begin{frame}
	\frametitle{\title_transformer}
	\begin{block}{Keep in mind}
		This is still all about performing math on language representations\uncover<2->{, no matter how complicated it gets.}
	\end{block}
\end{frame}


\begin{frame}
	\frametitle{\title_transformer}
	\centering
	\uncover<1->{\includegraphics[width=250pt]{nerd_stuff_1}}
	\uncover<2->{\includegraphics[width=250pt]{nerd_stuff_2}}
	\uncover<3->{\includegraphics[width=250pt]{nerd_stuff_3}}
\end{frame}


\begin{frame}
	\frametitle{\title_transformer}
	The key innovations of the Transformer are two:
	\begin{itemize}
		\item <2-> It no longer relies on sequential processing of inputs, which used to hinder parallelisation.
		\item <3-> It is able to apply Attention, more than once, in parallel!
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{\title_transformer}
	Given a phrase like the following:
	
	\begin{example}
		\textit{«Deux hommes entre tous les hommes ont le droit de répondre maintenant. Le capitaine Nemo et moi.»}
	\end{example}

	A transformer might be able to produce:
	\begin{itemize}
		\item <2->A representation that \textbf<2>{encodes the subject and the predicate of the sentence}.
		\item <3->A representation that \textbf<3>{establishes the concordance between a plural subject (« les deux hommes ») and the verb («~avoir~»~$\rightarrow$~«~ont~»).}
		\item <4->A representation that \textbf<4>{keeps track the gender of the subjects}.
		\item <5->A representation that \textbf<5>{learns that \textit{les deux hommes} are Captain Nemo and I.}
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{\title_transformer}
	\only<1,3>{
		\begin{block}{The Transformer was huge!}
		The Transformer can then put together all those representations into one, big, dense representation that encodes all of those linguistic features.
		\end{block}
	}

	\only<2>{\centering\includegraphics[scale=.4]{optimus_prime}}
\end{frame}


\def\title_bert{BERT: Bidirectional Encoder Representations from Transformers}


\begin{frame}
	\only<1,3->{\frametitle{\title_bert}}
	\only<2>{
		\centering\includegraphics[width=125pt]{bert}
		\frametitle{BERT: Bidirectional- oh who are we kidding}
	}

	\begin{itemize}
		\item <1,3-> BERT is a \textbf{language model} capable of producing richly-represented \textbf{contextual embeddings} of an input sequence.
		\item <3-> BERT is architecturally identical to a Transformer, trained in a very peculiar manner:
		\begin{itemize}
			\item <4-> Given a sequence, a word is randomly selected and removed from the sequence. BERT is trained to predict the word.
			\item <5-> Given a pair of sentences, the second sentence is masked and BERT is trained to predict the masked sentence.
		\end{itemize}
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{\title_bert}
	BERT gave way to a whole family of Transformer-based language models that tweaked and retrained the original using different techniques and datasets:
	\begin{itemize}
		\item <2-> RoBERTa
		\item <3->FlauBERT and CamemBERT
		\item <4->BART
		\item <5->BETO
	\end{itemize}
	\uncover<6->{And many more}
\end{frame}


\section{Concrete example} % (fold)
\label{sec:concrete_example}


\begin{frame}
	\frametitle{Code example}
	\centering
	\begin{overpic}[abs,unit=1mm,scale=.3]{code}
		\only<2->{\put(30,40){\includegraphics[width=80pt]{images/clown.png}}}
	\end{overpic}
\end{frame}


\begin{frame}
	\frametitle{Workflow: The process of matching a query and a document}
	Given a set of the raw texts of a series of queries and documents, we must train and fine-tune our model in order to determine which queries match which documents.
	\begin{itemize}
		\item<2->\textbf<2>{Tokenise the texts and convert them into sub-word units}
		\item<3->\textbf<3>{Pass them through the model}
		\item<4->\textbf<4>{If the prediction doesn't match the expectation, correct the model}
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Tokenising and indexing}
	\begin{example}
		Consider the following two texts, a query and a document: \\
		\begin{figure}
			
			\only<1>{« \textit{DC Circuits \& Systems Circuit Board DC Circuits \& Systems…} »}
			\only<2->{$[5,12197,21714,10,537,13442,\hdots,6]$}
		\end{figure}

		and

		\begin{figure}
			
			\only<1>{« \textit{Power Factor Correction Electricity and New Energy LabVolt…} »}
			\only<2->{$[5,8608,4208,8839,31094,21,14839,\hdots,6]$}
		\end{figure}

		\begin{itemize}
			\item <3-> The documents are split into \textbf{tokens}.
			\item <4-> Each token is assigned an index.
			\item <5-> The sentence is now a list of indices and is now ready to pass through the model.
		\end{itemize}

	\end{example}

\end{frame}


\begin{frame}
	\frametitle{Classifying the text with BERT}
	\begin{example}
	Let $\mathbf{q} = [5, 12197, \hdots, 6]$ and $\mathbf{d} = [5, 8608, \hdots, 6]$ be the query and the document represented as a list of indices, ie the lists of indices from the previous stage.

	\begin{center}
		\input{images/simple_bert}
	\end{center}

	\uncover<7->{Where $\mathbf{y}_{\text{pred}}$ can be either 1 or 0}
	\end{example}
\end{frame}


\begin{frame}
	\frametitle{Correcting the model}
	\begin{itemize}
		\item <1-> $\mathbf{y}_{\text{pred}}$ is compared to the expectation $\mathbf{y}_{\text{exp}}$. If there is a mismatch, we run a correction algorithm based on the concepts of \textbf{backpropagation} and \textbf{Stochastic Gradient Descent}.
		\item <2-> If $\mathbf{y}_{\text{pred}}$ and $\mathbf{y}_{\text{exp}}$ do match, then we move onto the next example in the dataset.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{And that's it!}
	Thank you for your attention!
\end{frame}
% section concrete_example (end)

% \begin{frame}
% \frametitle{Sample frame title}
% \begin{alertblock}{Remark}
% ola k ase
% \end{alertblock}

% \begin{example}
% 	hoal mundo
% \end{example}

% \end{frame}

\end{document}